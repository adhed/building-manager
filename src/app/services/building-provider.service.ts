import { Injectable } from '@angular/core';
import { mockedBuilding } from '@app/mocks/building';
import { Building } from '@app/models';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BuildingProviderService {

  public getBuilding(id: string): Observable<Building> {
    // TODO: implement fetching building by it's ID
    return of(mockedBuilding);
  }
}
