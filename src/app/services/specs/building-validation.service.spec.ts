import { BuildingValidationService } from '../building-validation.service';

describe('BuildingValidationService', () => {
  let service: BuildingValidationService;

  beforeEach(() => {
    service = new BuildingValidationService();
  });

  it('should consider nickname as invalid when it is not starting with a correct letter', () => {
    service.isValidNickname('bbbb')
      .subscribe((isValid) => {
        expect(isValid).toBe(false);
      });
  });

  it('should consider nickname as invalid when it is empty', () => {
    service.isValidNickname('')
      .subscribe((isValid) => {
        expect(isValid).toBe(false);
      });
  });

  it('should consider nickname as valid when it starts with a correct letter', () => {
    service.isValidNickname('a')
      .subscribe((isValid) => {
        expect(isValid).toBe(true);
      });
  });

  it('should consider nickname as valid when it start with a correct letter (even in uppercase)', () => {
    service.isValidNickname('A')
      .subscribe((isValid) => {
        expect(isValid).toBe(true);
      });
  });
});
