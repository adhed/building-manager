import {  Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BuildingValidationService {
  static readonly validNicknameStartingLetter = 'a';

  public isValidNickname(nickname: string): Observable<boolean> {
    const isValid = nickname.toLowerCase().startsWith(BuildingValidationService.validNicknameStartingLetter);
    return isValid ? of(true) : of(false);
  }
}
