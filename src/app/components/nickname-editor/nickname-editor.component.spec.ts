import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { Observable, of, Subject } from 'rxjs';
import { NicknameEditorComponent } from './nickname-editor.component';

describe('NicknameEditorComponent', () => {
  let component: NicknameEditorComponent;
  let fixture: ComponentFixture<NicknameEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NicknameEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NicknameEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show error below input only when this input is invalid', () => {
    const index = 0;
    const control = component.getFormControl(index);

    control.setErrors({ someError: true });

    expect(component.shouldShowControlError(index)).toBe(true);
  });

  it('should not show errors when control is valid', () => {
    const index = 0;
    const control = component.getFormControl(index);

    control.setErrors(null);

    expect(component.shouldShowControlError(index)).toBe(false);
  });

  it('should disable saving button initially', () => {
    component.ngOnInit();

    component.isSavingDisabled$
      .subscribe((isDisabled) => {
        expect(isDisabled).toBe(true);
      });
  });

  it('should enable saving button only if all entered nicknames are valid', () => {
    const mockedStatusChanges$ = new Subject<string>();

    component.ngOnInit();
    (component as any).formGroup = {
      statusChanges: mockedStatusChanges$ as Observable<string>,
    } as FormGroup;

    mockedStatusChanges$.next('VALID');
    mockedStatusChanges$.next('VALID');

    component.isSavingDisabled$
      .subscribe((isDisabled) => {
        expect(isDisabled).toBe(false);
      });
  });

  it('should disable saving button if some of nickname is invalid', () => {
    const mockedStatusChanges$ = new Subject<string>();

    component.ngOnInit();
    (component as any).formGroup = {
      statusChanges: mockedStatusChanges$ as Observable<string>,
    } as FormGroup;

    mockedStatusChanges$.next('INVALID');
    mockedStatusChanges$.next('INVALID');

    component.isSavingDisabled$
      .subscribe((isDisabled) => {
        expect(isDisabled).toBe(true);
      });
  });

  it('should add a new nickname row when the user will press enter', () => {
    const spy = spyOn(component as any, 'addNewNicknameControl');

    component.onEnterPressed();

    expect(spy).toHaveBeenCalled();
  });

  it('should add a new nickname row when the user will click on add new nickname button', () => {
    const spy = spyOn(component as any, 'addNewNicknameControl');

    component.onAddNewNicknameClick();

    expect(spy).toHaveBeenCalled();
  });

  it('should add a new nickname row when creating a form initially', () => {
    const spy = spyOn(component as any, 'addNewNicknameControl');

    component.ngOnInit();

    expect(spy).toHaveBeenCalled();
  });

  it('should correctly retrieve nickname control from FormArray', () => {
    const expectedValue = 'some nickname';

    (component as any).formGroup = new FormGroup({
      nicknames: new FormArray([
        new FormControl(''),
        new FormControl(expectedValue),
        new FormControl(''),
      ])
    });

    expect(component.getFormControl(1).value).toBe(expectedValue);
  });

  it('should emit nickname values on save button click', () => {
    const mockedNickname = 'some nickname2';
    const expectedValue = ['', mockedNickname, ''];
    const spy = spyOn(component.save, 'emit');

    (component as any).formGroup = new FormGroup({
      nicknames: new FormArray([
        new FormControl(''),
        new FormControl(mockedNickname),
        new FormControl(''),
      ])
    });

    component.onSaveBtnClick();

    expect(spy).toHaveBeenCalledWith(expectedValue);
  });
});
