import { ElementRef, EventEmitter, Output, QueryList, ViewChildren } from '@angular/core';
import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { AbstractControl, AsyncValidatorFn, FormArray, FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-nickname-editor',
  templateUrl: './nickname-editor.component.html',
  styleUrls: ['./nickname-editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NicknameEditorComponent implements OnInit {
  @ViewChildren('input') inputs: QueryList<ElementRef>;

  @Input() public inputPlaceholder = 'nickname';
  @Input() public inputLabel = 'Type a nickname';
  @Input() public formatValidator: AsyncValidatorFn;

  @Output() public save: EventEmitter<string[]> = new EventEmitter<string[]>();

  public isSavingDisabled$: Observable<boolean>;

  public formGroup: FormGroup;

  public get nicknamesControls(): AbstractControl[] {
    return this.nicknamesFormArray.controls;
  }

  private get nicknamesFormArray(): FormArray {
    return this.formGroup.get('nicknames') as FormArray;
  }

  ngOnInit(): void {
    this.createForm();

    this.isSavingDisabled$ = this.formGroup.statusChanges
      .pipe(
        map((status) => status !== 'VALID'),
        // startWith(true)
      );
  }

  public onNicknameDelete(index: number): void {
    this.nicknamesFormArray.removeAt(index);
  }

  public onAddNewNicknameClick(): void {
    this.addNewNicknameControl();
  }

  public onSaveBtnClick(): void {
    this.save.emit(this.nicknamesFormArray.getRawValue());
  }

  public shouldShowControlError(index: number): boolean {
    return this.nicknamesFormArray.at(index).invalid;
  }

  public getFormControl(index: number): AbstractControl {
    return this.nicknamesFormArray.at(index);
  }

  public onEnterPressed(): void {
    this.addNewNicknameControl();
  }

  private createForm(): void {
    this.formGroup = new FormGroup({
      nicknames: new FormArray([])
    });

    this.addNewNicknameControl();
  }

  private addNewNicknameControl(): void {
    this.nicknamesFormArray.push(
      new FormControl('', [], this.formatValidator)
    );

    setTimeout(() => this.inputs?.last.nativeElement.focus());
  }
}

