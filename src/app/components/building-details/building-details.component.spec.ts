import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BuildingValidationService } from '@app/services';
import { MockBuildingValidationService } from '@app/mocks';
import { BuildingDetailsComponent } from './building-details.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

describe('BuildingDetailsComponent', () => {
  let component: BuildingDetailsComponent;
  let fixture: ComponentFixture<BuildingDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        BuildingDetailsComponent
      ],
      imports: [
        MatSnackBarModule,
      ],
      providers: [
        { provide: BuildingValidationService, useClass: MockBuildingValidationService },
        { provide: ActivatedRoute,
          useValue: {
            data: of({ building: null })
          }
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildingDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render building title with its street and city', () => {
    const street = 'Washington Street';
    const city = 'New York';
    const expectedTitle = 'Building on Washington Street, New York';
    const building = { city, street, nicknames: [], country: '', description: '' };

    expect(component.getBuildingTitle(building)).toEqual(expectedTitle);
  });
});
