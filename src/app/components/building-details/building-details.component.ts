import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { AsyncValidatorFn } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { Building } from '@app/models';
import { BuildingValidationService } from '@app/services';
import { ValidateNicknameFormat } from '@app/validators';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-building-details',
  templateUrl: './building-details.component.html',
  styleUrls: ['./building-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BuildingDetailsComponent implements OnInit {
  public isEditMode = true;
  public building$: Observable<Building>;

  public readonly nicknamePlaceholder = 'Special building nickname';
  public readonly nicknameLabel = 'Type a building nickname';

  public get buildingNicknameFormatValidator(): AsyncValidatorFn {
    return ValidateNicknameFormat.createValidator(this.buildingValidationService);
  }

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly snackbar: MatSnackBar,
    private readonly buildingValidationService: BuildingValidationService,
  ) {}

  public ngOnInit(): void {
    this.building$ = this.activatedRoute.data
      .pipe(map((data: { building: Building }) => data.building));
  }

  public onNicknamesSaved(nicknames: string[]): void {
    this.snackbar.open(`We are saving ${nicknames?.length} nickname/s.`);
    console.log('Nicknames successfully validated and are ready to save:', nicknames);
  }

  public getBuildingTitle(building: Building): string {
    return `Building on ${building.street}, ${building.city}`;
  }
}
