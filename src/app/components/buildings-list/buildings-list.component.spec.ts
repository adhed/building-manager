import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BuildingDetailsComponent } from '../building-details';
import { BuildingsListComponent } from './buildings-list.component';

describe('BuildingsListComponent', () => {
  let component: BuildingsListComponent;
  let fixture: ComponentFixture<BuildingsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildingsListComponent ],
      imports: [
        RouterTestingModule.withRoutes([
          { path: 'building', component: BuildingDetailsComponent }
        ])
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildingsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
