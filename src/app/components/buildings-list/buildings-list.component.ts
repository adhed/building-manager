import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-buildings-list',
  templateUrl: './buildings-list.component.html',
  styleUrls: ['./buildings-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BuildingsListComponent implements OnInit {
  constructor(private readonly router: Router) {}

  public ngOnInit(): void {
    // TODO: implement fetching all buildings instead of mocked redirection
    this.navigateToMockedBuilding();
  }

  private navigateToMockedBuilding(): void {
    this.router.navigate(['building/222']);
  }
}
