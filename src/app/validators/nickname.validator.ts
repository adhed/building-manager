import { AbstractControl, AsyncValidatorFn } from '@angular/forms';
import { BuildingValidationService } from '@app/services';
import { timer } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

export class ValidateNicknameFormat {
  static readonly debounceTime = 500;

  static createValidator(buildingValidationService: BuildingValidationService): AsyncValidatorFn {
    return (control: AbstractControl) => {
        return timer(ValidateNicknameFormat.debounceTime)
          .pipe(switchMap(() => {
            return buildingValidationService.isValidNickname(control.value)
              .pipe(map((isValid: boolean) => isValid ? null : { format: true }));
            }));
    };
  }
}
