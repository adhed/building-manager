import { Building } from '@app/models';

export const mockedBuilding: Building = {
  country: 'United Kingdom',
  city: 'London',
  street: '30 St. Mary Axe',
  description: 'This is a really high and outrageous building. It is not easy to build someting like this, so prepare a special nickname for it.',
  nicknames: [],
};
