import { Building } from '@app/models';
import { Observable, of } from 'rxjs';

export class MockBuildingProviderService {
  public getBuilding(id: string): Observable<Building> {
    return of(null);
  }
}
