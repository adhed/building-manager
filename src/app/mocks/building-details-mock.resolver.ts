import { ActivatedRouteSnapshot } from '@angular/router';
import { Building } from '@app/models';
import { Observable, of } from 'rxjs';

export class MockBuildingDetailsResolver {
  public resolve(route: ActivatedRouteSnapshot): Observable<Building> {
    return of(null);
  }
}
