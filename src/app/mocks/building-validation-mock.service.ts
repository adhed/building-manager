import { Observable, of } from 'rxjs';

export class MockBuildingValidationService {
  public isValidNickname(nickname: string): Observable<boolean> {
    return of(!!nickname);
  }
}
