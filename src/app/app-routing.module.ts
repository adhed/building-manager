import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BuildingDetailsComponent } from './components';
import { BuildingsListComponent } from './components/buildings-list/buildings-list.component';
import { BuildingDetailsResolver } from './resolvers';

const routes: Routes = [
  {
    path: '',
    component: BuildingsListComponent,
    pathMatch: 'full',
  },
  {
    path: 'building/:buildingId',
    component: BuildingDetailsComponent,
    resolve: {
      building: BuildingDetailsResolver
    },
  },
  {
    path: '*',
    redirectTo: '',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
