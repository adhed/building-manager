export interface Building {
  city: string;
  country: string;
  description: string;
  nicknames: string[];
  street: string;
}
