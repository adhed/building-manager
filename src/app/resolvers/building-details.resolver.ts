import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Building } from '@app/models';
import { BuildingProviderService } from '@app/services';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BuildingDetailsResolver {
  constructor(private readonly buildingProvider: BuildingProviderService) {}

  public resolve(route: ActivatedRouteSnapshot): Observable<Building> {
    const { buildingId } = route.params;
    return this.buildingProvider.getBuilding(buildingId);
  }
}
